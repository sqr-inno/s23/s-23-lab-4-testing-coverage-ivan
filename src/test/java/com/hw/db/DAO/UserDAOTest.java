package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class UserDAOTest {

    private final JdbcTemplate jdbc = mock(JdbcTemplate.class);
    private final UserDAO userDAO = new UserDAO(jdbc);

    public static Stream<Object> provideArgumentsChange() {
        return Stream.of(
                Arguments.of(
                        new User("ivan", "ivan@email.com", "fullname", "about"),
                        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", null, "fullname", "about"),
                        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", null, "fullname", null),
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", null, null, "about"),
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", "ivan@email.com", null, "about"),
                        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", "ivan@email.com", "fullname", null),
                        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("ivan", "ivan@email.com", null, null),
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"
                )
        );
    }

    @ParameterizedTest
    @DisplayName("Testing Change for MC/DC coverage")
    @MethodSource("provideArgumentsChange")
    void testChange(User user, String sql) {
        userDAO.Change(user);

        verify(jdbc).update(eq(sql), ArgumentMatchers.<Object>any());
    }
}
