package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDAOTest {

    private final JdbcTemplate jdbc = mock(JdbcTemplate.class);
    private final ThreadDAO threadDAO = new ThreadDAO(jdbc);

    public static Stream<Arguments> provideArgumentsTreeSort() {
        return Stream.of(
                Arguments.of(
                        123, 1, 2, false,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"
                ),
                Arguments.of(
                        123, 1, 2, true,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"
                ),
                Arguments.of(
                        123, null, null, null,
                        "SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"
                )
        );
    }

    @ParameterizedTest
    @DisplayName("Testing treeSort for full statement coverage")
    @MethodSource("provideArgumentsTreeSort")
    void testTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String sql) {
        threadDAO.treeSort(id, limit, since, desc);

        verify(jdbc).query(eq(sql), any(PostDAO.PostMapper.class), any());
    }
}
