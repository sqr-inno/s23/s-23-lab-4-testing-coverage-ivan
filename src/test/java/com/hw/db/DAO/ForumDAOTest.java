package com.hw.db.DAO;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTest {

    private final JdbcTemplate jdbc = mock(JdbcTemplate.class);
    private final ForumDAO forumDAO = new ForumDAO(jdbc);

    private static Stream<Arguments> provideArgumentsForUserList() {
        return Stream.of(
                Arguments.of(10, "23.03.2023", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(10, "23.03.2023", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @DisplayName("Testing UserList for branch coverage")
    @MethodSource("provideArgumentsForUserList")
    void testUserList(Number limit, String since, Boolean desc, String sql) {
        forumDAO.UserList("slug", limit, since, desc);

        verify(jdbc).query(eq(sql), any(Object[].class), any(UserDAO.UserMapper.class));
    }
}
