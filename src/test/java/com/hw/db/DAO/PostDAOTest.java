package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class PostDAOTest {

    private final JdbcTemplate jdbc = mock(JdbcTemplate.class);
    private final PostDAO postDAO = new PostDAO(jdbc);

    public static Stream<Arguments> provideArgumentsSetPost() {
        return Stream.of(
                Arguments.of(
                        new Post(null, new Timestamp(0), "forum", null, null, null, true),
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post(null, new Timestamp(0), "forum", "message", null, null, true),
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author", null, "forum", null, null, null, true),
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post(null, null, "forum", "message", null, null, true),
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author", new Timestamp(0), "forum", "message", null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author", null, "forum", "message", null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author", new Timestamp(0), "forum", null, null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                )
        );
    }

    @ParameterizedTest
    @DisplayName("Testing setPost for full Basis path coverage")
    @MethodSource("provideArgumentsSetPost")
    void testSetPost(Post post, String sql) {
        Post existingPost = new Post();
        existingPost.setAuthor("author1");
        existingPost.setCreated(new Timestamp(1));
        existingPost.setMessage("message1");
        when(jdbc.queryForObject(any(), any(PostDAO.PostMapper.class), eq(123))).thenReturn(existingPost);

        postDAO.setPost(123, post);

        verify(jdbc).update(eq(sql), ArgumentMatchers.<Object>any());
    }
}
